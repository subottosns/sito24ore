# Sito 24 ore
Il nuovo sito della 24 ore, scritto con Flask+React.

## Istruzioni per l'uso (in attesa di docker)
Installate le dipendenze e libsubotto in devel.

Serviranno due terminali, uno nella cartella `backend` e uno nella cartella `frontend`.

Poi dovrete collegarvi all'indirizzo del frontend, il quale farà richieste al backend.

### Backend
* Creare un virtualenv, attivarlo e dare `pip install -r requirements.txt`
* Per lanciare il server. eseguire `FLASK_APP=main.py flask run`
* Dovreste avere il vostro backend in ascolto su `http://localhost:5000`

### Frontend
* Lanciare `npm install` per installare le dipendenze
* Per far partire il sito, dare `npm start`
* Collegatevi (sperabilemnte) a `http://localhost:3000`

## Endpoints

### Elenco edizioni
`GET /api/edizioni/`

Restituisce una lista delle edizioni passate:
```
[{"id": id, "anno": anno}]
```

### Informazioni edizione
`GET /api/edizione/<id>`

Restituisce le informazioni ("metadati") di quell'edizione

```
{
    "nome": nome,
    "squadre": [sq_1, sq_2],
    "capitani": [cpt_1, cpt_2],
    "vicecapitani": [vcpt_1, vcpt_2],
    "punteggio": [pts_1, pts_2],
    "inizio": inizio,
    "fine": fine,
    "luogo": luogo,
    "prima_coppia": [[def_1, atk_1], [def_2, atk_2]],
    "numero_partecipanti": [part_1, part_2],
}
```

### Statistiche PlayerMatch
`GET /api/playermatch/<id>`

Restituisce le statistiche di un PlayerMatch (una partecipazione di un giocatore a un'edizione)

```
{
    "giocatore": player_id,
    "partita": match_id,
    "squadra": team_id,
    "gf": pos_goals,
    "gs": neg_goals,
    "tempo": seconds,
    "turni": [turn_ids],
    "compagni": [(PlayerMatch compagno, gf, gs, tempo)],
    "avversari": [(PlayerMatch avversario, gf, gs, tempo)]
}
```

### Informazioni giocatore
`GET /api/giocatore/<id>`

Restituisce le informazioni di un giocatore

```
{
    "nome": fname,
    "cognome": lname,
    "commento": comment,
    "partecipazioni": [playermatch_ids]
}
```

### Statistiche aggregate edizione
`GET /api/statistiche/<id>`

Restituisce le statistiche "generali" di un'edizione, come elenco di PlayerMatch info

```
[
    {
        "nome": Player.fname,
        "cognome": Player.lname,
        "squadra": PlayerMatch.team_id
        "gf": pos_goals,
        "gs": neg_goals,
        "tempo": seconds
    }
]
```

### Statistiche aggregate totali
`GET /api/statistiche`

Identico a sopra, ma sommando i dati per tutte le 24ore passate

### Statistiche per coppie
`GET /api/coppie/<id>`

Restituisce le statistiche delle coppie di un'edizione, come elenco di informazioni sulle coppie

```
[
    {
        "difensore": Player difensore,
        "attaccante": Player attaccante,
        "squadra": team_id
        "gf": pos_goals,
        "gs": neg_goals,
        "tempo": seconds
    }
]
```

### Informazioni turno
`GET /api/turno/<id>`

Restituisce le informazioni di un singolo turno

```
{
    "partita": match_id,
    "inizio": inizio,
    "fine": fine,
    "risultato": [pts_1, pts_2],
    "giocatori": [[def_1, atk_1], [def_2, atk_2]]
}
```

### Elenco turni
`GET /api/turni/<edizione>`

Restituisce l'elenco dei turni di un'edizione della 24h

```
{
    "turni": [turns]
}
```

### Stato parziale
`GET /api/stato`

Restituisce lo stato attuale di una 24ore.

TODO: sono dati aggregati, o l'elenco (raw) degli eventi?

### Eventi
`GET /api/goals/<id>`

Restituisce l'elenco dei goal con timestamp; per fare i grafici

```
[
    {
        "tempo": timestamp,
        "squadra": team_id,
        "tipo": normale/supergoal
    }
]
```
## WS
Protocollo websocket per i dati della 24ore corrente.

Anche il subtracker userà un websocket
