import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import NavLink from 'react-router-dom/NavLink'
import NavItem from 'react-bootstrap/NavItem'
import { Nav, Navbar, Dropdown } from 'react-bootstrap';
import { Form, FormControl} from 'react-bootstrap'
import Button from 'react-bootstrap/Button'

export const NavigationBar = () => (
    <Navbar expand="lg" className="navbar-24ore">
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse>
        <Nav>
            <NavLink className="navlink" to="/home">Home</NavLink>
            <NavLink className="navlink" to="/edizione-corrente">Edizione corrente</NavLink>
            <NavLink className="navlink" to="/edizioni-passate">Edizioni passate</NavLink>
            <NavLink className="navlink" to="/trailer-sigla">Trailer e sigla</NavLink>
            <NavLink className="navlink" to="/manifestazione">La manifestazione</NavLink>
        </Nav>
    </Navbar.Collapse>
    </Navbar>
)