import React, { Component } from 'react';

function Title(props) {
    return (
        <div className="title-box" style={{backgroundColor: props.color}}>
            <div className="title">{props.name}</div>
        </div>
    );
}

function Box(props) {
    return(
        <div className="box-container" style={{width: props.width, float: props.float}}>
            <Title name={props.title} color={props.color}></Title>
            <div className="box-content"> {props.children} </div>
        </div>
    )
}

export default Box;