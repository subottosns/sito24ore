import React from 'react';
import Box from './Box';
import Table from 'react-bootstrap/Table'
import BootstrapTable from 'react-bootstrap-table-next';
 
const Home = (props) => {

    return (
       <div className="page-content">
           <Box title="Edizioni della 24ore" color="red" width="100%">
               <Table striped bordered hover size='sm' responsive='lg' style={{fontSize: '12px'}}>
                   <thead style={{textAlign: 'center'}}>
                       <th>Edizione</th>
                       <th>Data</th>
                       <th>Matematici</th>
                       <th colSpan='2'>Risultato</th>
                       <th>Fisici</th>
                       <th>Luogo</th>
                   </thead>
                   <tbody>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>X</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>9-10 febbraio 2019</td>
                           <td>
                                Capitano: Davide Gori<br/>
                                Vicecapitano: Lorenzo Demeio<br/>
                                Partecipanti ufficiali: 52
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1606</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1145</td>
                            <td>
                                Capitano: Enrico Negri<br/>
                                Vicecapitano: Luca Onorato<br/>
                                Partecipanti ufficiali: 74
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Palazzo della Carovana</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>IX</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>8-9 febbraio 2018</td>
                           <td>
                                Capitano: Riccardo Zanotto<br/>
                                Vicecapitano: Davide Gori<br/>
                                Partecipanti ufficiali: 57
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1320</td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1414</b></td>
                            <td>
                                Capitano: Alberto Bordin<br/>
                                Vicecapitano: Enrico Negri<br/>
                                Partecipanti ufficiali: 67
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Carducci</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>VIII</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>25-26 gennaio 2017</td>
                           <td>
                                Capitano: Matteo Migliorini<br/>
                                Vicecapitano: Riccardo Zanotto<br/>
                                Partecipanti ufficiali: 57
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1514</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1344</td>
                            <td>
                                Capitano: Alberto Bordin<br/>
                                Vicecapitano: Giacomo Petrillo<br/>
                                Partecipanti ufficiali: 64
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Carducci</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>VII</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1-2 febbraio 2016</td>
                           <td style={{wordBreak: 'normal'}}>
                                Capitano: Lorenzo Benedini<br/>
                                Vicecapitano: Matteo Migliorini<br/>
                                Partecipanti ufficiali: 58
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1707</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1415</td>
                            <td>
                                Capitani: Alberto Bordin,<br/>
                                Dario Ascari, Giacomo Petrillo<br/>
                                Partecipanti ufficiali: 49
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Carducci</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>VI</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>19-20 febbraio 2015</td>
                           <td>
                                Capitano: Federico Scavia<br/>
                                Vicecapitano: Lorenzo Benedini<br/>
                                Partecipanti ufficiali: 53
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1706</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1330</td>
                            <td>
                                Capitano: Marco Cilibrasi<br/>
                                Vicecapitano: Vasco Cavina,<br/> Enis Belgacem, Giuliano Chiriacò<br/>
                                Partecipanti ufficiali: 61
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Carducci</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>V</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>28-29 gennaio 2014</td>
                           <td>
                                Capitano: Alessandro Iraci<br/>
                                Vicecapitano: Federico Scavia<br/>
                                Partecipanti ufficiali: 69
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1511</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1377</td>
                            <td>
                                Capitano: Giulio Mandorli<br/>
                                Vicecapitano: Marco Cilibrasi<br/>
                                Partecipanti ufficiali: 45
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Carducci</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>IV</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>11-12 febbraio 2013</td>
                           <td>
                                Capitano: Giovanni Paolini<br/>
                                Vicecapitano: Alessandro Iraci<br/>
                                Partecipanti ufficiali: 53
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1519</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1458</td>
                            <td>
                                Capitano:  Luca Rigovacca<br/>
                                Vicecapitano: Giulio Mandorli<br/>
                                Partecipanti ufficiali: 43
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Carducci</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>III</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>14-15 febbraio 2012</td>
                           <td>
                                Capitano: Giovanni Paolini<br/>
                                Vicecapitano: Giulio Bresciani<br/>
                                Partecipanti ufficiali: 53
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1865</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1442</td>
                            <td>
                                Capitano: Luca Rigovacca<br/>
                                Vicecapitano: Stefano Bolzonella<br/>
                                Partecipanti ufficiali: 63
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Carducci</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>II</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>4-5 aprile 2011</td>
                           <td>
                                Capitano: Fabrizio Bianchi<br/>
                                Vicecapitano: Gennady Uraltsev<br/>
                                Partecipanti ufficiali: 39
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1658</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1398</td>
                            <td>
                                Capitano: Stefano Bolzonella<br/>
                                Vicecapitano: Enrico Morgante<br/>
                                Partecipanti ufficiali: 37
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Timpano</td>
                       </tr>
                       <tr>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>I</td>
                           <td style={{textAlign: 'center', verticalAlign: 'middle'}}>3-4 marzo 2010</td>
                           <td>
                                Capitano: Fabrizio Bianchi<br/>
                                Vicecapitano: Gennady Uraltsev<br/>
                                Partecipanti ufficiali: 27
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}><b>1636</b></td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>1616</td>
                            <td>
                                Capitano: Stefano Bolzonella<br/>
                                Vicecapitano: Enrico Morgante<br/>
                                Partecipanti ufficiali: 24
                            </td>
                            <td style={{textAlign: 'center', verticalAlign: 'middle'}}>Collegio Timpano</td>
                       </tr>
                   </tbody>
               </Table>
           </Box>
           <Box title="@24oresns" color="yellow" width="50%" float="left">
               <a class="twitter-timeline" href="https://twitter.com/24oresns?ref_src=twsrc%5Etfw" height="500" width="500">Tweets by 24oresns</a>
           </Box>
           <Box title="test" color="blue" width="20%" float="right">
               {props.state.status}
           </Box>
       </div>
    );
}
 
export default Home;