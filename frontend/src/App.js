import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { NavigationBar } from './components/NavigationBar';
import Home from './components/Home';
import EdizioneCorrente from './components/EdizioneCorrente';

const $ = window.$;

class App extends Component {

  updateScore() {
    var self = this;
    $.ajax({
        type: 'post',
        url: "http://localhost:8080/score",
        data: JSON.stringify({
          action: 'get'
        }),
        contentType: 'application/json',
        success: function(data) {
            self.setState((prevState) => {
                let score = prevState.score;
                score = data;
                return {score}
            })
        },
        error: function(jqXHR,textStatus,errorThrown) {
            console.log("Error");
        }
    });
  }

  constructor(props) {
    super(props);
    this.state = {score: ''};
    this.updateScore = this.updateScore.bind(this);
  }

  componentDidMount() {
    this.updateScore();
  }

  render() {
    return(
      <React.Fragment>
        <Router>
          < NavigationBar />
          {this.state.score.status}
          <Switch>
            <Redirect exact from="/" to="/home"></Redirect>
            <Route path="/home" render={() => <Home state={this.state.score}></Home>}></Route>
            <Route path="/edizione-corrente" component={EdizioneCorrente}></Route>
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
