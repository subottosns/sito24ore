from flask_marshmallow import Marshmallow
from marshmallow import post_load
from libsubotto.data import Match, Team, Player
ma = Marshmallow()


class TeamSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Team
        fields = ("id", "name")


class PlayerSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Player
        fields = ("id", "fname", "lname")


class MatchSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Match
        fields = ("id", "name", "begin", "end", "year", "place", "team_a", "team_b",)
    team_a = ma.Pluck(TeamSchema, "name")
    team_b = ma.Pluck(TeamSchema, "name")



match_schema = MatchSchema()
matches_schema = MatchSchema(many=True)
