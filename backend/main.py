from flask import Flask, Blueprint
from flask_cors import CORS
from flask_restful import Api
import json

from resources import db, MatchListResource
from schemas import ma #, match_schema, matches_schema

app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://subotto:pwdsubotto@localhost:5433/subotto'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
ma.init_app(app)

api_bp = Blueprint('api', __name__, url_prefix="/api")
api = Api(api_bp)
api.add_resource(MatchListResource, '/edizioni')

app.register_blueprint(api_bp)

# TODO: websocket blueprint


@app.route('/')
def hello_world():
    return "Ciao"
