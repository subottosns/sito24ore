from flask_restful import Resource
from flask_sqlalchemy import SQLAlchemy as fSQLAlchemy
from schemas import matches_schema
from libsubotto.data import Match, Team

db = fSQLAlchemy()


class MatchListResource(Resource):
    def get(self):
        posts = db.session.query(Match).all()
        return matches_schema.dump(posts)
